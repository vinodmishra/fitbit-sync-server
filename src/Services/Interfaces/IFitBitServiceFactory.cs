﻿using FitbitSyncServer.Model;

namespace FitbitSyncServer.Services.Interfaces;

public interface IFitbitServiceFactory
{
    IFitbitService GetFitbitService(Account account);
}