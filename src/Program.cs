using System.ComponentModel.DataAnnotations;
using FitbitSyncServer.Components;
using FitbitSyncServer.Domain;
using FitbitSyncServer.Services;
using FitbitSyncServer.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorComponents().AddInteractiveServerComponents();
builder.Services.AddDbContextPool<AccountDbContext>(o =>
    o.UseSqlite($"Data Source={Environment.GetEnvironmentVariable("DATA_DIR") ?? ""}Accounts.db"));
builder.Services.AddScoped<IDataService, DataService>();
builder.Services.AddScoped<IAccountsService, AccountsService>();
builder.Services.AddScoped<IFitbitServiceFactory, FitbitServiceFactory>();
builder.Services.AddScoped<IFitbitLoggingService, FitbitLoggingService>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();
using var scope = app.Services.CreateScope();
var dbContext = scope.ServiceProvider.GetService<AccountDbContext>();
dbContext?.Database.EnsureCreated();
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error", createScopeForErrors: true);
}
app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseAntiforgery();
app.UseSwagger();
app.UseReDoc();
app.MapRazorComponents<App>().AddInteractiveServerRenderMode();

app.MapPost("/api/scale-measurements",
    async ([FromQuery][Required] string accountName,
        [FromQuery] double weight,
        [FromQuery] double? fat,
        [FromQuery] int? impedance,
        [FromQuery] DateTime? date,
        [FromServices]IFitbitLoggingService fitbitLoggingService) =>
    {
        if (impedance.HasValue)
            await fitbitLoggingService.LogMeasurementsAsync(accountName, date ?? DateTime.Now, weight, impedance.Value);
        else
            await fitbitLoggingService.LogMeasurementsAsync(accountName, date ?? DateTime.Now, weight, fat);
    })
    .WithName("Post Weight and Fat measurement")
    .WithOpenApi(); 
app.Run();