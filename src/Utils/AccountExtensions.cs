﻿using Fitbit.Api.Portable;
using Fitbit.Api.Portable.OAuth2;
using FitbitSyncServer.Model;
using Newtonsoft.Json;

namespace FitbitSyncServer.Utils;

public static class AccountExtensions
{
    public static FitbitAppCredentials GetCredentials(this Account account)
    {
        return new FitbitAppCredentials
        {
            ClientId = account.ClientId,
            ClientSecret = account.ClientSecret
        };
    }


    public static OAuth2AccessToken GetToken(this Account account)
    {
        return JsonConvert.DeserializeObject<OAuth2AccessToken>(account.AccessToken);
    }
}