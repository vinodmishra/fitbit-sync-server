﻿using FitbitSyncServer.Model;
namespace FitbitSyncServer.Services;

public static class ImpedanceCalculationService
{
    public static double? CalculateBodyFat(Account account, double weight, int? impedance)
    {
        if (account == null || !impedance.HasValue || account.Height == 0)
            return null;

        var age = GetAge(account.BirthDate);
        var lbm = GetLbm(age, account.Height, weight, impedance.Value);
        return GetFatPercentage(account.Gender, age, weight, account.Height, lbm);
    }

    private static int GetAge(DateTime dateOfBirth)
    {
        var today = DateTime.Today;
        var age = today.Year - dateOfBirth.Year;
        if (dateOfBirth.Date > today.AddYears(-age)) age--;
        return age;
    }

    private static double GetLbm(int age, double height, double weight, int impedance)
    {
        var lbm = (height * 9.058f / 100) * (height / 100);
        lbm += weight * 0.32f + 12.226f;
        lbm -= impedance * 0.0068f;
        lbm -= age * 0.0542f;
        return lbm;
    }

    private static double GetFatPercentage(Gender gender, int age, double weight, double height, double lbm)
    {
        double coefficient = 1.0f;
        var constValue = gender == Gender.Female ? age <= 49 ? 9.25f : 7.25f : 0.8f;
        if (gender == Gender.Female)
        {
            coefficient = weight switch
            {
                > 60 => 0.96f,
                < 50 => 1.02f,
                _ => coefficient
            };
            if (height > 160 && weight is < 50 or > 60)
            {
                coefficient *= 1.03f;
            }
        }
        else
        {
            if (weight < 61)
            {
                coefficient = 0.98f;
            }
        }
        var fatPercentage = (1.0f - ((lbm - constValue) * coefficient) / weight) * 100;
        if (fatPercentage > 63)
        {
            fatPercentage = 75;
        }
        return CheckValueConstraints(fatPercentage, 5, 75);
    }

    private static double CheckValueConstraints(double value, double min, double max)
    {
        if (value < min)
            return min;
        
        return value > max ? max : value;
    }
}


