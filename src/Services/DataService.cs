﻿using FitbitSyncServer.Domain;
using FitbitSyncServer.Model;
using FitbitSyncServer.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FitbitSyncServer.Services;

public class DataService : IDataService
{
    private readonly ILogger<DataService> _logger;
    private readonly AccountDbContext _context;

    public DataService(ILogger<DataService> logger, AccountDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    public async Task<Account> GetAccountAsync(string name)
    {
        return await _context.Accounts.FindAsync(name);
    }

    public Task<List<Account>> GetAccountsAsync()
    {
        return _context.Accounts.OrderBy(o => o.Name).ToListAsync();
    }

    public async Task<bool> DeleteAccountAsync(string name)
    {
        try
        {
            var account = await _context.Accounts.FindAsync(name);
            if (account == null)
            {
                _logger.LogWarning("Account does not exist");
                return false;
            }
            _context.Remove(account);
            await _context.SaveChangesAsync();
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Delete Failed");
            return false;
        }
    }
        

    public async Task<bool> CreateAccountAsync(Account account)
    {
        try
        {
            await _context.Accounts.AddAsync(account);
            await _context.SaveChangesAsync();
            return true;
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Create Failed");
            return false;
        }
    }

    public Task SaveChangesAsync() => _context.SaveChangesAsync();
}