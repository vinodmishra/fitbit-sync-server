﻿namespace FitbitSyncServer.Model;

public enum Gender
{
    Male,
    Female
}