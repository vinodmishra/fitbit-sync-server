﻿using System.ComponentModel.DataAnnotations;

namespace FitbitSyncServer.Model;

public class Account
{
    [Required(ErrorMessage = "The Name field is Required")]
    public string Name { get; set; }

    public DateTime BirthDate { get; set; } = DateTime.Now;
    
    public double Height { get; set; }
    
    public Gender Gender { get; set; }

    [Required(ErrorMessage = "The ClientId field is Required")]
    public string ClientId { get; set; }

    [Required(ErrorMessage = "The ClientSecret field is Required")]
    public string ClientSecret { get; set; }
    public string BaseUrl { get; set; }
    public string AccessToken { get; set; }
}