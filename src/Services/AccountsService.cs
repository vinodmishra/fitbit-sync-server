﻿using FitbitSyncServer.Model;
using FitbitSyncServer.Services.Interfaces;
using Fitbit.Api.Portable.OAuth2;
using FitbitSyncServer.Utils;
using Newtonsoft.Json;

namespace FitbitSyncServer.Services;

public class AccountsService : IAccountsService
{
    private readonly IDataService _dataService;
    private static readonly string[] Scopes = { "profile", "weight"};

    public AccountsService(IDataService dataService)
    {
        _dataService = dataService;
    }

    public Task<List<Account>> GetAccountsAsync()
    {
        return _dataService.GetAccountsAsync();
    }

    public Task<bool> DeleteAccountAsync(string name)
    {
        return _dataService.DeleteAccountAsync(name);
    }

    public async Task<string> CreateAccountAsync(Account account)
    {
        await _dataService.CreateAccountAsync(account);
        var fitbitAppCredentials = account.GetCredentials();
        var authenticator = new OAuth2Helper(fitbitAppCredentials, GetRedirectUrl(account.BaseUrl));
        return authenticator.GenerateAuthUrl(Scopes, account.Name);
    }

    public async Task CallbackAsync(string code, string accountName)
    {
        var account = await _dataService.GetAccountAsync(accountName);
        var fitbitAppCredentials = account.GetCredentials();
        var authenticator = new OAuth2Helper(fitbitAppCredentials, GetRedirectUrl(account.BaseUrl));
        var accessToken = await authenticator.ExchangeAuthCodeForAccessTokenAsync(code);
        account.AccessToken = JsonConvert.SerializeObject(accessToken);
        await _dataService.SaveChangesAsync();
    }

    private static string GetRedirectUrl(string url)
    {
        if (!url.EndsWith('/'))
            url += "/";
        return url + "fitbit/callback";
    }
}