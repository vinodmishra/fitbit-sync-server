﻿using FitbitSyncServer.Model;
using Microsoft.EntityFrameworkCore;

namespace FitbitSyncServer.Domain;

public sealed class AccountDbContext : DbContext
{
    public AccountDbContext(DbContextOptions<AccountDbContext> options)
        : base(options)
    {
    }
    public DbSet<Account> Accounts { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<Account>()
            .HasKey(o => o.Name);
    }
}