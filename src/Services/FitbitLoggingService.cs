﻿using Fitbit.Api.Portable;
using FitbitSyncServer.Model;
using FitbitSyncServer.Services.Interfaces;

namespace FitbitSyncServer.Services;

public class FitbitLoggingService : IFitbitLoggingService
{
    private readonly IFitbitServiceFactory _fitbitServiceFactory;
    private readonly IDataService _dataService;

    public FitbitLoggingService(
        IFitbitServiceFactory fitbitServiceFactory, 
        IDataService dataService)
    {
        _fitbitServiceFactory = fitbitServiceFactory;
        _dataService = dataService;
    }

    public async Task LogMeasurementsAsync(string accountName, DateTime dateTime, double weight, double? fat)
    {
        var account = await _dataService.GetAccountAsync(accountName);
        if (account == null || string.IsNullOrWhiteSpace(account.AccessToken))
            return;
        await LogMeasurementsAsync(dateTime, account, weight, fat);
    }

    public async Task LogMeasurementsAsync(string accountName, DateTime dateTime, double weight, int? impedance)
    {
        var account = await _dataService.GetAccountAsync(accountName);
        if (account == null || string.IsNullOrWhiteSpace(account.AccessToken))
            return;
        
        var fat = ImpedanceCalculationService.CalculateBodyFat(account, weight, impedance);

        await LogMeasurementsAsync(dateTime, account, weight, fat);
    }

    private async Task LogMeasurementsAsync(DateTime dateTime, Account account, double weight, double? fat)
    {
        var fitbitService = _fitbitServiceFactory.GetFitbitService(account);

        await fitbitService.LogWeightAndFatAsync(dateTime, weight, fat);

        account.AccessToken = fitbitService.GetToken();

        await _dataService.SaveChangesAsync();
    }
}