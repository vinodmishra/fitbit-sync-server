﻿using Microsoft.EntityFrameworkCore;

namespace FitbitSyncServer.Domain;

public static class AccountDbContextFactory
{
    public static AccountDbContext Create(string connectionString)
    {
        var optionsBuilder = new DbContextOptionsBuilder<AccountDbContext>();
        optionsBuilder.UseSqlite(connectionString);

        var context = new AccountDbContext(optionsBuilder.Options);
        context.Database.EnsureCreated();

        return context;
    }
}