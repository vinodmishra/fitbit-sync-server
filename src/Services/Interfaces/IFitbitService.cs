﻿using Fitbit.Api.Portable.OAuth2;

namespace FitbitSyncServer.Services.Interfaces;

public interface IFitbitService
{
    string GetToken();
    Task LogWeightAndFatAsync(DateTime date, double weight, double? fat);
}