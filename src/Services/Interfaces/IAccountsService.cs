﻿using FitbitSyncServer.Model;

namespace FitbitSyncServer.Services.Interfaces;

public interface IAccountsService
{
    Task<List<Account>> GetAccountsAsync();
    Task<bool> DeleteAccountAsync(string name);
    Task<string> CreateAccountAsync(Account account);
    Task CallbackAsync(string code, string accountName);
}