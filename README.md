Fitbit Sync Server
=========================================================================
This server allows you to connect multiple fitbit accounts and assign them a name which in turn can be used by Home Assistant to send data from weight sensors to Fitbit .e.g Xiaomi Composition Scale 2. Following steps outline what needs to be done to get it working

How to run the server
-------------------------------------------------------------------------
Currently only docker or docker-compose is supported. The server exposes port 8080 but that needs to be routed through a 
SSL reverse proxy as fitbit requires SSL.
/data is the volume where the container stores the database.

Given below is an example. Please replace the path to local config.

Docker-compose:
```yaml
version: '3'
services:
  fitbitsyncserver:
    image: registry.gitlab.com/vinodmishra/fitbit-sync-server:latest
    container_name: fitbitsyncserver
    ports:
      - 8080:8080
    restart: unless-stopped
    volumes:
      - /path/to/local/config:/data
```
Docker:
```bash
docker run --name fitbitsyncserver -p 8080:8080 --restart unless-stopped -v /path/to/local/config:/data registry.gitlab.com/vinodmishra/fitbit-sync-server:latest
```

Instructions on setting up the server and connecting it to Home Assistant
-------------------------------------------------------------------------

*   To start with this server needs to be accessible via HTTPS. Fitbit won't allow you to authorise without that. I suggest using a reverse proxy like Nginx Proxy Manager.
*   Please create an app on[fitbit dev account](https://dev.fitbit.com/apps). The fields that matter are Application Type, Redirect URL and Access Type.
    *   Application Type must be Personal
    *   Redirect url will be wherever you are hosting this server appended to '/fitbit/callback' e.g. https://192.168.1.10:8080/fitbit/callback
*   The above step needs to repeated for every user that wants to send data from Home Assistant and the application needs to be created from their fitbit account.
*   Once that is done, please use the add account feature on this server to add a unique name for that user along with client id and secret of the application created on fitbit. Please not that birth date, age and gender are only required if you have a scale that shows impedance on home assistant and you would like to record body fat data on fitbit.
*   This server exposes an API that allows you to send data to fitbit. Documentation for the API can be found [here](/api-docs).
*   Setup a [RESTful command](https://www.home-assistant.io/integrations/rest_command) integration on home assistant for this API by adding it to configuration.yaml. Example below
```yaml    
    rest_command:
      fitbit_logger:
        url: https://fitbitsync.home/api/scale-measurements?accountName={{accountName}}&weight={{weight}}&impedance={{impedance}}
        method: POST
        verify_ssl: false
 ```        
    
*   Create automation that sends data to this rest command when your sensor detects weight. To differentiate between people you can use weight ranges or just completely different sensors if that is not an option. Example below
```yaml    
    alias: Send Weight for John
    trigger:
      - platform: state
        entity_id:
          - sensor.mi_body_composition_scale_8846_mass
    condition:
      - condition: numeric_state
        entity_id: sensor.mi_body_composition_scale_8846_mass
        above: 70
    action:
      - service: rest_command.fitbit_logger
        data:
          accountName: John
          weight: "{{states.sensor.mi_body_composition_scale_8846_mass.state|float}}"
          impedance: "{{states.sensor.mi_body_composition_scale_8846_impedance.state|int}}"
    mode: single
 ```     