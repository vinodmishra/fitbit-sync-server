﻿namespace FitbitSyncServer.Services.Interfaces;

public interface IFitbitLoggingService
{
    Task LogMeasurementsAsync(string accountName, DateTime dateTime, double weight, double? fat);
    Task LogMeasurementsAsync(string accountName, DateTime dateTime, double weight, int? impedance);
}