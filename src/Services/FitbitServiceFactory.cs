﻿using Fitbit.Api.Portable;
using FitbitSyncServer.Model;
using FitbitSyncServer.Services.Interfaces;
using FitbitSyncServer.Utils;

namespace FitbitSyncServer.Services;

public class FitbitServiceFactory : IFitbitServiceFactory
{
    private readonly ILoggerFactory _factory;

    public FitbitServiceFactory(ILoggerFactory factory)
    {
        _factory = factory;
    }

    public IFitbitService GetFitbitService(Account account)
    {
        var fitbitClient = new FitbitClient(account.GetCredentials(), account.GetToken());
        var logger = _factory.CreateLogger<FitbitService>();
        return  new FitbitService(fitbitClient, logger);
    }
}