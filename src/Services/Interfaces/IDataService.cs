﻿using FitbitSyncServer.Model;

namespace FitbitSyncServer.Services.Interfaces;

public interface IDataService
{
    Task<Account> GetAccountAsync(string accountName);
    Task<List<Account>> GetAccountsAsync();
    Task<bool> DeleteAccountAsync(string name);
    Task<bool> CreateAccountAsync(Account account);
    Task SaveChangesAsync();
}