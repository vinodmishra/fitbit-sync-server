﻿using Fitbit.Api.Portable;
using Fitbit.Api.Portable.OAuth2;
using FitbitSyncServer.Services.Interfaces;
using Newtonsoft.Json;

namespace FitbitSyncServer.Services;

public class FitbitService : IFitbitService
{
    private readonly FitbitClient _fitbitClient;
    private readonly ILogger<FitbitService> _logger;

    public FitbitService(FitbitClient fitbitClient, ILogger<FitbitService> logger)
    {
        _fitbitClient = fitbitClient;
        _logger = logger;
    }

    public string GetToken() => JsonConvert.SerializeObject(_fitbitClient.AccessToken);

    public async Task LogWeightAndFatAsync(DateTime date, double weight, double? fat)
    {
        try
        {
            await _fitbitClient.RefreshOAuth2TokenAsync();
            await _fitbitClient.LogWeightAsync(date, weight);
            if (fat != null)
            {
                await _fitbitClient.LogFatAsync(date, fat.Value);
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Unable to log fat and weight");
        }
    }
}